<?php

/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 30.04.2017
 * Time: 9:09
 * @property app\models\Project $project
 */

namespace app\services;

use app\models\Doer;
use app\models\DoerMiddleMarks;
use Yii;
use app\models\Project;
use app\models\Criterions;
use app\models\Mark;
use app\models\DoerProject;
use yii\web\NotFoundHttpException;

class MarksManager
{
    public $project;

    /**
     * MarksManager constructor.
     * @param app\models\Project $project
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Создает оценки для проекта с привязкой к исполнителю
     * @param $doer_id integer
     */
    public function updateMarks($doer_id)
    {
        $criterions = Criterions::find()->all();
        $doer = Doer::findOne($doer_id);
        if($doer == null)
            throw new NotFoundHttpException('Запрашиваемого испонителя не существует');
        $professions = $doer->getProfessionsId();

        foreach ($professions as $profession_id)
        {
            /** @var app\models\Criterions $criterion */
            foreach ($criterions as $criterion) {
                /** @var app\models\Mark $mark */
                $mark = new Mark();
                $mark->project_id = $this->project->id;
                $mark->criterion_id = $criterion->id;
                $mark->doer_id = $doer_id;
                $mark->profession_id = $profession_id;
                $mark->mark = 0;
                $mark->save();
            }
        }
    }

    /**
     * Создает новые записи app\models\Mark для проектов
     * (создание критерия)
     * @param $criterion_id
     */
    public static function updateProjectsByCriterion($criterion_id)
    {
        $doerProjects = DoerProject::find()->all();
        /** @var \app\models\DoerProject $doerProject */
        foreach ($doerProjects as $doerProject)
        {
            $profs = $doerProject->doer->getProfessionsId();
            foreach ($profs as $prof_id)
            {
                $mark = new Mark();
                $mark->doer_id = $doerProject->doer_id;
                $mark->project_id = $doerProject->project_id;
                $mark->criterion_id = $criterion_id;
                $mark->profession_id = $prof_id;
                $mark->mark = 0;
                $mark->save();
            }
        }

    }

    /**
     * Оцениваем проект и выставляем среднее арфмитичесикое проекту
     * @param $request
     */
    public function applyMarks($request)
    {
        $post = $request->post();
        $sum = 0;
        $count = 0;

        $professions = [];
//return dd($request);
        foreach ($post['Mark'] as $id => $value) {
            $mark = Mark::find()->where(['id' => $id])->one();

            $mark->mark = $value;

            if(!isset($professions[$mark->profession_id]))
            {
                $professions[$mark->profession_id] = [$mark];
            } else {
                array_push($professions[$mark->profession_id], $mark);
            }

            $mark->save();
            if ($value!=0) { // считаем, если указали оценку
                $sum = $sum + $value;
                $count++;
            }
        }


//        return dd($professions);
        foreach ($professions as $profession_id => $marks)
        {
            $sum = 0;
            $count = 0;

            $marks = Mark::find()->where(['doer_id' => $marks[0]->doer_id, 'profession_id' => $profession_id])->all();

            foreach($marks as $mark)
            {
                $sum = $sum+$mark->mark;

                if ($mark->mark != 0)
                    $count++;
            }

            $doerMiddleProfMark = DoerMiddleMarks::find()->where(['doer_id' => $marks[0]->doer_id, 'profession_id' => $profession_id])->one();

            if($doerMiddleProfMark == null)
            {
                $doerMiddleProfMark = new DoerMiddleMarks();

                $doerMiddleProfMark->doer_id = $marks[0]->doer_id;
                $doerMiddleProfMark->profession_id = $profession_id; // ??
            }


            if ($count>0) {
                $doerMiddleProfMark->middle_mark = $sum/$count;//round($sum/$count, 1);
                $doerMiddleProfMark->save();


            }
        }
        $professionAvgCount = 0;
        $professionAvgSumm = 0;

        foreach ($professions as $profession_id => $marks) {
            $doerMiddleProfMark = DoerMiddleMarks::find()->where(['doer_id' => $marks[0]->doer_id, 'profession_id' => $profession_id])->one();

            if ($doerMiddleProfMark==null) {
                $doerMiddleProfMark = new DoerMiddleMarks();

                $doerMiddleProfMark->doer_id = $marks[0]->doer_id;
                $doerMiddleProfMark->profession_id = $profession_id;
            }

            if ($doerMiddleProfMark->middle_mark != 0) {
                $professionAvgCount++;
                $professionAvgSumm += $doerMiddleProfMark->middle_mark;
            }
        }

        $avgBall = null;
        $doer = Doer::find()->where(['id' => $post['doer_id']])->one();
        if ($professionAvgCount > 0) {
//            return dd($professionAvgSumm);
            $avgBall = $professionAvgSumm / $professionAvgCount;

            $doer->middle_mark = $avgBall;

            $doer->save();
        }

//        $this->project->save();

//        $this->updateDoerRating($this->project->doer);

    }

    public static function recountRating($doer_id)
    {
        $doerMiddleMarks = DoerMiddleMarks::find()->where(['doer_id' => $doer_id])->all();

        foreach ($doerMiddleMarks as $middleMark)
        {
            $marks = Mark::find()->where(['doer_id' => $doer_id, 'profession_id' => $middleMark->profession_id])->all();
            $sum = 0;
            $count = 0;

            foreach($marks as $mark)
            {
                if ($mark->mark!=0) {
                    $sum = $sum+$mark->mark;
                    $count++;
                }
            }

            if ($count>0) {
                $middleMark->middle_mark = $sum/$count;
            }

            $middleMark->save();
        }
    }

    /**
     * Выставляем средний бал исполнителю
     * @param $doer
     */
    public function updateDoerRating($doer)
    {
        $projects = $doer->projects;

        $sum = 0;
        $count = 0;
        foreach ($projects as $project)
        {
            if ($project->middle_mark != 0) {
                $sum = $sum + $project->middle_mark;
                $count++;
            }
        }

        if ($count>0) {
            $doer->middle_mark = $sum/$count;//round($sum/$count, 1);
        }

        $doer->save();
    }

}