﻿<?php

return [
    'Comments' => 'Примечания',
	'Text' => 'Текст',	
	'Add comment' => 'Добавить примечания',
	'Post comment' => 'Добавить примечания',
	'Cancel' => 'Отмена',
	'Reply' => 'Ответить',
	'Edit' => 'Изменить',
	'Delete' => 'Удалить',
	'Updated at {date-relative}' => 'Обновлено {date-relative}',
	'Comment was deleted.' => 'Примечания был удален.',
];
