<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mark`.
 */
class m170429_202406_create_mark_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mark', [
            'id' => $this->primaryKey(),
            'mark' => $this->float()->notNull(),
            'criterion_id' => $this->integer()->notNull(),
            'project_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-mark-project_id',
            'mark',
            'project_id'
        );

        $this->addForeignKey(
            'fk-mark-project_id',
            'mark',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-mark-criterion_id',
            'mark',
            'criterion_id'
        );

        $this->addForeignKey(
            'fk-mark-criterion_id',
            'mark',
            'criterion_id',
            'criterions',
            'id',
            'CASCADE'
        );
        
        


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mark');
    }
}
