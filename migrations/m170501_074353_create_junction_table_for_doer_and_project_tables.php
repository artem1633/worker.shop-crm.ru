<?php

use yii\db\Migration;

/**
 * Handles the creation of table `doer_project`.
 * Has foreign keys to the tables:
 *
 * - `doer`
 * - `project`
 */
class m170501_074353_create_junction_table_for_doer_and_project_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('doer_project', [
            'id' => $this->primaryKey(),
            'doer_id' => $this->integer(),
            'project_id' => $this->integer(),
        ]);

        // creates index for column `doer_id`
        $this->createIndex(
            'idx-doer_project-doer_id',
            'doer_project',
            'doer_id'
        );

        // add foreign key for table `doer`
        $this->addForeignKey(
            'fk-doer_project-doer_id',
            'doer_project',
            'doer_id',
            'doer',
            'id',
            'CASCADE'
        );

        // creates index for column `project_id`
        $this->createIndex(
            'idx-doer_project-project_id',
            'doer_project',
            'project_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-doer_project-project_id',
            'doer_project',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `doer`
        $this->dropForeignKey(
            'fk-doer_project-doer_id',
            'doer_project'
        );

        // drops index for column `doer_id`
        $this->dropIndex(
            'idx-doer_project-doer_id',
            'doer_project'
        );

        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-doer_project-project_id',
            'doer_project'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            'idx-doer_project-project_id',
            'doer_project'
        );

        $this->dropTable('doer_project');
    }
}
