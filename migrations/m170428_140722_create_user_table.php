<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'status' => $this->integer()->notNull(),
            'email' => $this->string()->notNull(),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true),
            'phone' => $this->string(),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'password' => 'admin',
            'status' => 10,
            'email' => 'admin@yandex.ru',
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
