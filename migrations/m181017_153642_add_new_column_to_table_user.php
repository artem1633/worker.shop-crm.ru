<?php

use yii\db\Migration;

/**
 * Class m181017_153642_add_new_column_to_table_user
 */
class m181017_153642_add_new_column_to_table_user extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
            $this->addColumn('user','role',$this->string()->defaultValue('user'));
    }

    public function down()
    {
        echo "m181017_153642_add_new_column_to_table_user cannot be reverted.\n";

        return false;
    }

}
