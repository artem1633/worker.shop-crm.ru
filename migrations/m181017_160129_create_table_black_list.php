<?php

use yii\db\Migration;

/**
 * Class m181017_160129_create_table_black
 */
class m181017_160129_create_table_black_list extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('black_list', [
            'id' => $this->primaryKey(),
            'from_id' => $this->integer(),
            'block_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        echo "m181017_160129_create_table_black cannot be reverted.\n";

        return false;
    }

}
