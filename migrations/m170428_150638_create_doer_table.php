<?php

use yii\db\Migration;

/**
 * Handles the creation of table `doer`.
 */
class m170428_150638_create_doer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('doer', [
            'id' => $this->primaryKey(),
            'snp' => $this->string()->notNull()->unique(),
            'phone' => $this->string(),
            'email' => $this->string()->unique(),
            'skype' => $this->string(),
            'city_id' => $this->integer(),
            'education' => $this->string(),
            'main_professions' => $this->text(),
            'other_professions' => $this->text(),
            'specific' => $this->text(),
            'comment' => $this->text(),
            'middle_mark' => $this->float(),
            'projects_count' => $this->integer()->defaultValue(0)
        ]);

        $this->createIndex(
            'idx-doer-city_id',
            'doer',
            'city_id'
        );

        $this->addForeignKey(
            'fk-doer-city_id',
            'doer',
            'city_id',
            'cities',
            'id',
            'CASCADE'
        );



    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('doer');
    }
}
