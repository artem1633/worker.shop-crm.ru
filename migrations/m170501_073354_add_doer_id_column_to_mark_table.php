<?php

use yii\db\Migration;

/**
 * Handles adding doer_id to table `mark`.
 */
class m170501_073354_add_doer_id_column_to_mark_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('mark', 'doer_id', $this->integer());

        $this->createIndex(
            'idx-mark-doer_id',
            'mark',
            'doer_id'
        );

        $this->addForeignKey(
            'fk-mark-doer_id',
            'mark',
            'doer_id',
            'doer',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('mark', 'doer_id');
    }
}
