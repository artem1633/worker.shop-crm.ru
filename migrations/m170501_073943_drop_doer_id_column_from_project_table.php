<?php

use yii\db\Migration;

/**
 * Handles dropping doer_id from table `project`.
 */
class m170501_073943_drop_doer_id_column_from_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // drops foreign key for table `category`
        $this->dropForeignKey(
            'fk-project-doer_id',
            'project'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            'idx-project-doer_id',
            'project'
        );

        $this->dropColumn('project', 'doer_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('project', 'doer_id', $this->integer());

        $this->createIndex(
            'idx-project-doer_id',
            'project',
            'doer_id'
        );

        $this->addForeignKey(
            'fk-project-doer_id',
            'project',
            'doer_id',
            'doer',
            'id',
            'CASCADE'
        );
    }
}
