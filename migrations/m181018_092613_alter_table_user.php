<?php

use yii\db\Migration;

/**
 * Class m181018_092613_alter_table_user
 */
class m181018_092613_alter_table_user extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->update('user',['role' => 'admin'], ['login' => 'admin']);
    }

    public function down()
    {
        echo "m181018_092613_alter_table_user cannot be reverted.\n";

        return false;
    }

}
