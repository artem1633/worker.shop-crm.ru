<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170429_152348_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'doer_id' => $this->integer()->notNull(),
            'user_creator_id' => $this->integer()->notNull(),
            'middle_mark' => $this->float()->notNull()->defaultValue(0),
            'date_created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'date_updated' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',

        ]);

        $this->createIndex(
            'idx-project-doer_id',
            'project',
            'doer_id'
        );

        $this->addForeignKey(
            'fk-project-doer_id',
            'project',
            'doer_id',
            'doer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-project-user_creator_id',
            'project',
            'user_creator_id'
        );

        $this->addForeignKey(
            'fk-project-user_creator_id',
            'project',
            'user_creator_id',
            'user',
            'id',
            'CASCADE'
        );
        
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
