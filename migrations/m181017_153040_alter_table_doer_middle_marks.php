<?php

use yii\db\Migration;

/**
 * Class m181017_153040_alter_table_DoerMiddleMarks
 */
class m181017_153040_alter_table_doer_middle_marks extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('doer_middle_marks','middle_mark',$this->integer());
    }

    public function down()
    {
        echo "m181017_153040_alter_table_doer_middle_marks cannot be reverted.\n";

        return false;
    }

}
