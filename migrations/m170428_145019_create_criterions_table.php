<?php

use yii\db\Migration;

/**
 * Handles the creation of table `criterions`.
 */
class m170428_145019_create_criterions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('criterions', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('criterions');
    }
}
