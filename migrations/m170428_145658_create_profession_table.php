<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profession`.
 */
class m170428_145658_create_profession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('profession', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('profession');
    }
}
