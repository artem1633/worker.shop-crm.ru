<?php

use yii\db\Migration;

/**
 * Class m181018_065645_comment_from
 */
class m181018_065645_comment_from extends Migration
{
    public function up()
    {
        $this->addColumn('{{%comment}}', 'from', $this->string()->after('entity'));
    }

    public function down()
    {
        $this->dropColumn('{{%comment}}', 'from');
    }
}
