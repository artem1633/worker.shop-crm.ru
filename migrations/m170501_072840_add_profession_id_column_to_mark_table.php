<?php

use yii\db\Migration;

/**
 * Handles adding profession_id to table `mark`.
 */
class m170501_072840_add_profession_id_column_to_mark_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('mark', 'profession_id', $this->integer());

        $this->createIndex(
            'idx-mark-profession_id',
            'mark',
            'profession_id'
        );

        $this->addForeignKey(
            'fk-mark-profession_id',
            'mark',
            'profession_id',
            'profession',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('mark', 'profession_id');
    }
}
