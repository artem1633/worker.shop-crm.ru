<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doer".
 *
 * @property integer $id
 * @property string $snp
 * @property string $phone
 * @property string $email
 * @property string $skype
 * @property integer $city_id
 * @property string $education
 * @property integer $main_professions
 * @property string $other_professions
 * @property string $specific
 * @property string $comment
 * @property double $middle_mark
 * @property integer $projects_count
 *
 * @property Cities $city
 * @property Profession $mainProfession
 */
class Doer extends \yii\db\ActiveRecord
{
    public $criterions;
    public $black;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doer';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {//если заказ завершен то ставим дату изменения на дату фактическую
            $this->other_professions = json_encode($this->other_professions);
            $this->main_professions = json_encode($this->main_professions);

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->other_professions = json_decode($this->other_professions);
        $this->main_professions = json_decode($this->main_professions);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snp', 'email'], 'required'],
            [['city_id', 'projects_count'], 'integer'],
            [['specific', 'comment'], 'string'],
            [['middle_mark'], 'number'],
            [['snp', 'phone', 'email', 'skype', 'education'], 'string', 'max' => 255],
            [['snp'], 'unique'],
            [['email'], 'unique'],
            [['other_professions', 'main_professions','criterions'], 'safe'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snp' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Email',
//            'skype' => 'Скайп',
            'middle_mark' => 'Средний бал',
            'city_id' => 'Город',
            'education' => 'Образование',
            'main_professions' => 'Основная специализация',
            'other_professions' => 'второстепенные специализации',
            'specific' => 'Узкая специализация',
            'comment' => 'Примечание',
            'projects_count' => 'Количество проектов',
            'black' => 'В черный список'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getCity()
//    {
//        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['doer_id' => 'id']);
    }


    /**
     * Получаем id всех профессий (основных и второстепенных) исполнителя
     * @return array
     */
    public function getProfessionsId()
    {
        $mainProfessions = $this->main_professions;
        return array_unique(array_merge($mainProfessions, $this->other_professions ? $this->other_professions : []));
    }

    /**
     * Возвращает список моделей городов
     * @return mixed
     */
    public function getCitiesList()
    {
        return ArrayHelper::map(Cities::find()->all(), 'id', 'name');
    }

    /**
    * Возвращает список моделей специализаций
    * @return mixed
    */
    public function getProfessionList()
    {
        return ArrayHelper::map(Profession::find()->all(), 'id', 'name');
    }

    /**
     * Возвращает список моделей специализаций
     * @return mixed
     */
    public function getProfessionListForSearch()
    {
        $array = ArrayHelper::map(Profession::find()->all(), 'id', 'name');
        $newArray = [];

        foreach ($array as $key => $value){
            $newArray['"'.$key.'"'] = $value;
        }

        return $newArray;
    }

    public function getFreeProjects()
    {
        $doerProjects = DoerProject::find()->where(['doer_id' => $this->id])->groupBy('project_id')->all();
        $freeProjects = Project::find();

        foreach($doerProjects as $project)
        {
            $freeProjects->andWhere(['!=', 'id', $project->project_id]);
        }


        return ArrayHelper::map($freeProjects->all(), 'id', 'name');
    }

    public function getCriterions(){
            return ArrayHelper::map(Criterions::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasMany(Mark::className(), ['doer_id' => 'id']);
    }

    public function getMarksByProject($project_id)
    {
        return Mark::find()->where(['doer_id' => $this->id, 'project_id' => $project_id])->all();
    }


}
