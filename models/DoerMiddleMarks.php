<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doer_middle_marks".
 *
 * @property integer $id
 * @property integer $doer_id
 * @property integer $profession_id
 * @property double $middle_mark
 *
 * @property Doer $doer
 * @property Profession $profession
 */
class DoerMiddleMarks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doer_middle_marks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doer_id', 'profession_id'], 'required'],
            [['doer_id', 'profession_id'], 'integer'],
            [['middle_mark'], 'number','min' => 1, 'max' => 10],
            [['doer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doer::className(), 'targetAttribute' => ['doer_id' => 'id']],
            [['profession_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profession::className(), 'targetAttribute' => ['profession_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doer_id' => 'Doer ID',
            'profession_id' => 'Profession ID',
            'middle_mark' => 'Middle Mark',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoer()
    {
        return $this->hasOne(Doer::className(), ['id' => 'doer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfession()
    {
        return $this->hasOne(Profession::className(), ['id' => 'profession_id']);
    }
}
