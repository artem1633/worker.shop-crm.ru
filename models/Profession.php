<?php

namespace app\models;

use Yii;
use app\models\Doer;

/**
 * This is the model class for table "profession".
 *
 * @property integer $id
 * @property string $name
 */
class Profession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profession';
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            // Да это новая запись (insert)
//            $doerProjects = DoerProject::find()->all();
//            $criterions = Criterions::find()->all();
//            /** @var \app\models\DoerProject $doerProject */
//            foreach ($doerProjects as $doerProject)
//            {
//                foreach($criterions as $criterion)
//                {
//                    $mark = new Mark();
//                    $mark->doer_id = $doerProject->doer_id;
//                    $mark->project_id = $doerProject->project_id;
//                    $mark->criterion_id = $criterion->id;
//                    $mark->profession_id = $this->id;
//                    $mark->mark = 0;
//                    $mark->save();
//                }
//            }
        } else {

        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $doers = Doer::find()->all();

        foreach ($doers as $doer)
        {
            $newMainProfessions = [];
            $newOtherProfessions = [];

            $mainProfessions = $doer->main_professions;
            $otherProfessions = $doer->other_professions;

            if($mainProfessions != null)
            {
                foreach ($mainProfessions as $id)
                {
                    if($id != $this->id)
                    {
                        array_push($newMainProfessions, $id);
                    }
                }
            }

            if($otherProfessions != null)
            {
                foreach ($otherProfessions as $id)
                {
                    if($id != $this->id)
                    {
                        array_push($newOtherProfessions, $id);
                    }
                }
            }


            $doer->main_professions = $newMainProfessions;
            $doer->other_professions = $newOtherProfessions;
            $doer->save();
        }



    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }
}
