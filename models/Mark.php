<?php

namespace app\models;

use app\services\MarksManager;
use Yii;

/**
 * This is the model class for table "mark".
 *
 * @property integer $id
 * @property double $mark
 * @property integer $criterion_id
 * @property integer $project_id
 * @property integer $profession_id
 * @property integer $doer_id
 * @property Criterions $criterion
 * @property Project $project
 */
class Mark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mark', 'criterion_id', 'project_id', 'profession_id', 'doer_id'], 'required'],
            [['mark'], 'number'],
            [['criterion_id', 'project_id'], 'integer'],
            [['criterion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Criterions::className(), 'targetAttribute' => ['criterion_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark' => 'Mark',
            'criterion_id' => 'Criterion ID',
            'project_id' => 'Project ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCriterion()
    {
        return $this->hasOne(Criterions::className(), ['id' => 'criterion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfession()
    {
        return $this->hasOne(Profession::className(), ['id' => 'profession_id']);
    }
}
