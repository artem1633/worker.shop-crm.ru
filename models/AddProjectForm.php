<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 02.05.2017
 * Time: 6:46
 */

namespace app\models;


use yii\base\Exception;

class AddProjectForm extends \yii\base\Model
{
    public $project_id;
    public $doer_id;

    public function __construct($doer_id)
    {
        $this->doer_id = $doer_id;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['project_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Проект',
        ];
    }

    /**
     * Добавляет проект пользователю
     * @return bool
     */
    public function addProject()
    {
        if($this->project_id == null)
            throw new Exception('Проект не найден');

        $doerProject = new DoerProject();
        $doerProject->project_id = $this->project_id;
        $doerProject->doer_id = $this->doer_id;
        return $doerProject->save();
    }
}