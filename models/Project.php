<?php

namespace app\models;

use Yii;

use app\services\MarksManager;

use app\models\DoerProject;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $doer_id
 * @property integer $user_creator_id
 * @property string $date_created
 * @property string $date_updated
 *
 * @property Doer $doer
 * @property User $userCreator
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_updated'], 'required'],
            [['user_creator_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 255],
//            [['user_creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_creator_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            $this->date_updated = date('Y-m-d H:i:s', time());
            $this->user_creator_id = Yii::$app->user->identity->id;

            return true;
        }
        return false;
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        parent::afterDelete();

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'doer_id' => 'Исполнитель',
            'user_creator_id' => 'Создатель',
            'date_created' => 'Дата создания',
            'date_updated' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarks()
    {
        return $this->hasMany(Mark::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoerProjects()
    {
        return $this->hasMany(DoerProject::className(), ['project_id' => 'id']);
    }

    public function getDoers()
    {
        $doerProjects = $this->doerProjects;

        $doers = Doer::find();
        foreach ($doerProjects as $doerProject)
        {
            $doers->andFilterWhere(['id' => $doerProject->doer_id]);
        }
    }


    /**
     * Вычисляем среднее арифметическое оценок
     * @return float|int
     */
    public function getMarksAvg()
    {
        $marks = $this->marks;

        $marksSum = 0;
        $marksCount = count($marks);
        foreach($marks as $mark)
        {
            $marksSum = $marksSum + $mark->mark();
        }

        return $marksSum/$marksCount;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'user_creator_id']);
    }
}
