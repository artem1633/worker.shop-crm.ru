<?php

namespace app\models;

use rmrevin\yii\module\Comments\interfaces\CommentatorInterface;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password_hash
 * @property string $password
 * @property integer $status
 * @property string $email
 * @property integer $is_deletable
 * @property string $phone
 *
 * @property Project[] $projects
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface, \rmrevin\yii\module\Comments\interfaces\CommentatorInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['login'], 'unique'],
            [['status', 'is_deletable'], 'integer'],
            [['login', 'password_hash', 'password', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function beforeDelete()
    {
        parent::beforeDelete();

        if($this->is_deletable == false)
        {
            return false;
        } else {
            return true;
        }

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->status = 0;
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'status' => 'Статус',
            'email' => 'Email',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['user_creator_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }

    /**
     * @inheritdoc
     */
    public function getAdmin()
    {
        if (Yii::$app->user->identity->permmission == "admin"){
            return true;
        } else {
            return false;
        }
    }
    public function getFreeze()
    {
        return $this->freeze_balance;
    }
    public function getSetting()
    {
        return Setting::find()->where(['id' => '1'])->one();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function getCommentatorAvatar()
    {
        return '@web/images/no-avatar.png';
    }

    public function getCommentatorName()
    {
        return $this->login;
    }

    public function getCommentatorUrl()
    {
        return ['/profile', 'id' => $this->id]; // or false, if user does not have a public page
    }
}
