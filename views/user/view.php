<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>
<div class="user-view">

    <?php if($model->role == 'admin'):?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'password_hash',
            'password',
            'status',
            'email:email',
            'is_deletable',
            'phone',
        ],
    ]) ?>
    <?php endif;?>

    <?php if($model->role == 'user'):?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'status',
                'email:email',
                'is_deletable',
                'phone',
            ],
        ]) ?>
    <?php endif;?>

</div>
