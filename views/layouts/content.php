<?php
use yii\widgets\Breadcrumbs;

?>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <div class="content" id="content">
    
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1 class="page-header">Basic Tables</h1>
        <?php } else { ?>
        <?php } ?>

<!--         <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?> -->

        <?= $content ?>
    </div>

