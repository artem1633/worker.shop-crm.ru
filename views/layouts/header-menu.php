<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 19.04.2017
 * Time: 23:36
 */

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Исполнители', 'icon' => 'fa  fa-rouble', 'url' => ['/doer'],],
                    ['label' => 'Проекты', 'icon' => 'fa  fa-list', 'url' => ['/project'],],
                    ['label' => 'Справочники', 'icon' => 'fa  fa-align-justify', 'url' => 'javascript:;',
                        'options' => [
                            'class' => 'has-sub',
                        ],
                        'items' => [
                            ['label' => 'Города', 'url' => ['/cities']],
                            ['label' => 'Специализации', 'url' => ['/profession']],
                            ['label' => 'Критерии оценки', 'url' => ['/criterions']],
                        ],
                    ],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user', 'url' => ['/user'],],
                    ['label' => 'Черный список', 'icon' => 'fa  fa-ban', 'url' => ['/black-list'],],
                ],
            ]
        );
    ?>
</div>
