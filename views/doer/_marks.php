<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 02.05.2017
 * Time: 17:36
 */

$doerMiddleMarks = \app\models\DoerMiddleMarks::find()->where(['doer_id' => $doer_id])->all();

?>

<table id="w0" class="table table-striped table-bordered detail-view"><tbody>
    <?php foreach($doerMiddleMarks as $middleMark): ?>
        <tr><th><?=$middleMark->profession->name?></th><td><?=$middleMark->middle_mark?></td></tr>
    <?php endforeach; ?>
</tbody></table>,
