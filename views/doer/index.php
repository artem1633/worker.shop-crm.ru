<?php
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Исполнители";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse doer-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Исполнители</h4>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin(['id' => 'find-main-pref']); ?>
        <div style="float: left">
        <?= $form->field($model, 'main_professions')->widget(Select2::classname(), [
            'data' => $model->getProfessionList(),

            'options' => ['placeholder' => 'Выберите ...', 'id' => 'find-proff', 'value' => $id],
            'pluginEvents' => [
                "change" => "function() 
						{
                            $('#find-main-pref').submit();
						}",
            ],
            'pluginOptions' => [
                'width' => '150px',

                'allowClear' => true,
                'multiple' => false,
            ],
        ])->label('поиск по специализациям');?>
        <?php
        if(Yii::$app->request->post('Doer')['main_professions']){
            if(empty($doerFind)){
                echo '<div>Ничего не найдено</div>';
            }
        }
        ?>
        </div>

            <div style="float: left; margin-left: 50px">
                <?= $form->field($model, 'criterions')->widget(Select2::classname(), [
                    'data' => $model->getCriterions(),

                    'options' => ['placeholder' => 'Выберите ...', 'id' => 'find-crit', 'value' => $crit],
                    'pluginEvents' => [
                        "change" => "function() 
						{
						    $('#find-main-pref').submit();
						}",
                    ],
                    'pluginOptions' => [
                        'width' => '150px',

                        'allowClear' => true,
                        'multiple' => false,
                    ],
                ])->label('поиск по критериям');?>


            </div>

        <?php ActiveForm::end(); ?>



        <div class="clearfix"></div>
        <div id="ajaxCrudDatatable">


            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
            ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Добавить исполнителя','class'=>'btn btn-default']).
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
            '{toggleData}'.
            '{export}'
            ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [//  With selected    Удалить
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            ["bulk-delete"] ,
            [
            "class"=>"btn btn-danger btn-xs",
            'role'=>'modal-remote-bulk',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            ]),
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>

        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
