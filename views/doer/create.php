<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Doer */

?>
<div class="doer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
