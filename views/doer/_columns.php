<?php
use app\models\Cities;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '15px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '20px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'snp',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'middle_mark',
        'width' => '10px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'education',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'main_professions',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'other_professions',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'specific',
    // ],
    //[
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'comment',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'main_professions',
        'value' => function($model){

            $id = (is_array($model)) ? $model['id'] : $model->id;
            if(is_array($model)) {
               $ot_pref = json_decode($model['main_professions']);
            }else{
                $ot_pref = $model['main_professions'];
            }

            $mark = '';
            if(!empty($ot_pref)){
            foreach($ot_pref as $pr){
              $profName =  \app\models\Profession::find()->select('*')->where(['id' => $pr])->one();

                $doerMiddleMarks = \app\models\DoerMiddleMarks::find('*')->where(['profession_id' => $profName->id])->andWhere(['doer_id' => $id])->one();
                        if($doerMiddleMarks == null){
                            $mark .= ' '.$profName->name.'(0)';
                        }else{
                            $mark .= ' '.$profName->name.'('.$doerMiddleMarks->middle_mark.'),';
                        }


            }
            }

            return $mark;
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getProfessionListForSearch(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'other_professions',
        'value' => function($model){
            $id = (is_array($model)) ? $model['id'] : $model->id;
            if(is_array($model)) {
                $ot_pref = json_decode($model['other_professions']);
            }else{
                $ot_pref = $model['other_professions'];
            }

            $mark = '';
            if(!empty($ot_pref)){
                foreach($ot_pref as $pr){
                    $profName =  \app\models\Profession::find()->select('*')->where(['id' => $pr])->one();
                    $doerMiddleMarks = \app\models\DoerMiddleMarks::find('middle_mark')->where(['profession_id' => $profName->id])->andWhere(['doer_id' => $id])->one();
                    if($doerMiddleMarks == null){
                        $mark .= ' '.$profName->name.'(0)';
                    }else{
                        $mark .= ' '.$profName->name.'('.$doerMiddleMarks->middle_mark.'),';
                    }
                }
            }
            return $mark;
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getProfessionListForSearch(),

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'projects_count',
        'width' => '5px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city_id',
        'value' => function($model){
            $id = (is_array($model)) ? $model['city_id'] : $model->city_id;

            $city = Cities::find()->where(['id' => $id])->one();

            return $city['name'];
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> $model->getCitiesList(),
        'header' => 'Город',

    ],
    [

        'attribute'=>'black',
        'content' => function($model){

                $bl = \app\models\BlackList::findOne(['block_id' => $model->id]);
                if($bl){
                    return '<b class="text-danger">Уже В черном списке!</b>';
                }
            return Html::a('<span class="fa fa-ban"> В черный список</span>', ['black-list/pluse','id' => $model->id]);
        },
        'format' => 'raw',



    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Сохранить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'],
    ],

];   