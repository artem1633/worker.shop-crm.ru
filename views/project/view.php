<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$tabModelMarls = [];

foreach($modelMarks as $mark)
{
    if(!isset($tabModelMarls[$mark->profession_id]))
    {
        $tabModelMarls[$mark->profession_id] = [$mark];
    } else {
        array_push($tabModelMarls[$mark->profession_id], $mark);
    }
}


?>
<div class="project-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'userCreator.login',
                'label' => 'Логин создателя',
            ],
            'date_created',
            'date_updated',
        ],
    ]) ?>


    <?php $markForm = ActiveForm::begin(); ?>

    <input class="form-control" type="hidden" name="doer_id" value="<?=$doer_id?>">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <?php $counter = 1; foreach($tabModelMarls as $prof_id => $marks): ?>
                <?php if($counter == 1){ ?>
                    <li class="active"><a href="#tab_<?=$prof_id?>" data-toggle="tab"><?=$marks[0]->profession->name?></a></li>
                <?php } else { ?>
                    <li><a href="#tab_<?=$prof_id?>" data-toggle="tab"><?=$marks[0]->profession->name?></a></li>
                <?php } ?>
            <?php $counter++; endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php $counter = 1; foreach($tabModelMarls as $prof_id => $marks): ?>
                <div class="tab-pane <?= $counter == 1 ? "active" : "" ?>" id="tab_<?=$prof_id?>">
                    <?php foreach($marks as $mark): ?>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><?=$mark->criterion->name?></label>
                            <input class="form-control" type="number" min="1" max="10" name="Mark[<?=$mark->id?>]" placeholder="<?php if($mark->mark){echo $mark->mark;}else{ echo "от 1 до 10";} ?>">
                        </div>
                    <?php $counter++; endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- /.tab-content -->
    </div>

    <div class="form-group">
        <?php if(count($modelMarks) != 0): ?>
            <?= Html::submitButton('Выставить оценки', ['class' => 'btn btn-warning']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
