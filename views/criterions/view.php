<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Criterions */
?>
<div class="criterions-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
