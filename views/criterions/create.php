<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Criterions */

?>
<div class="criterions-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
