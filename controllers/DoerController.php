<?php

namespace app\controllers;

use app\models\AddProjectForm;
use app\models\Cities;
use app\models\DoerProject;
use app\models\Project;
use app\models\ProjectSearch;
use app\models\User;
use app\services\MarksManager;
use Yii;
use app\models\Doer;
use app\models\DoerSearch;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Mark;
use app\models\DoerMiddleMarks;
use app\models\Criterions;

/**
 * DoerController implements the CRUD actions for Doer model.
 */
class DoerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Doer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Doer();
        $searchModel = new DoerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $id = '';
        $crit = '';


       if(Yii::$app->request->post('Doer')){
         $post =  Yii::$app->request->post('Doer');

           $id = $post['main_professions'];
           $crit = $post['criterions'];


           $dataProvider->query
               ->leftJoin('mark', 'mark.doer_id = doer.id')
               ->groupBy('doer_id')
               ->orderBy(['mark'=> SORT_DESC]);

           if($crit != null)
           {
               $dataProvider->query->andFilterWhere(['criterion_id' => $crit]);
           }else{
               $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
           }

           if($id != null)
           {
               $dataProvider->query->andFilterWhere(['or', ['like', 'main_professions', '"'.$id.'"'], ['like', 'other_professions', '"'.$id.'"']]);
           }

           $headers = Yii::$app->response->headers;
//           Yii::info($headers->get('content-type'), 'doer');
//           if ($headers->get('Expires')!=null)
//               $headers = $headers->remove('Expires');
//           if ($headers->get('Cache-Control')!=null)
//               $headers = $headers->remove('Cache-Control');
//           if ($headers->get('Pragma')!=null)
//               $headers = $headers->remove('Pragma');
//           if ($headers->get('Last-Modified')!=null)
//               $headers = $headers->remove('Last-Modified');
       }

//        session_start();
        header_remove("Expires");
        header_remove("Cache-Control");
        header_remove("Pragma");
        header_remove("Last-Modified");

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
            'crit' => $crit,
            'model' => $model
        ]);
    }

    /**
     * DEPRECATED
     * Привязывает проект к пользователю
     * @param $doer_id
     * @param $project_id
     * @return bool
     */
    public function actionAddProject($doer_id, $project_id)
    {
        $project = Project::findOne($project_id);
        if($project == null)
            throw new NotFoundHttpException('Запрашиваемого проекта не существует.');

        $marksManager = new MarksManager($project);
        $marksManager->updateMarks($doer_id);

        $doerProject = new DoerProject();
        $doerProject->saveRelation($doer_id, $project_id);

        return true;
    }

    /**
     * Displays a single Doer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $user = User::findOne(['id' => Yii::$app->user->getId()]);
        $ordersSearchModel = new ProjectSearch();
        $ordersDataProvider = $ordersSearchModel->searchWithDoer(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'ordersSearchModel' => $ordersSearchModel,
            'ordersDataProvider' => $ordersDataProvider,
            'doer_id' => $id,
            'user' => $user,
        ]);
    }

    public function actionTest($id)
    {
        $model = $this->findModel($id);

        echo $model->getProfessionsId();
        exit;
    }

    /**
     * Creates a new Doer model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Doer();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить исполнителя",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить исполнителя",
                    'content'=>'<span class="text-success">Создание исполнителя успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить исполнителя",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    public function actionAdd($id)
    {
        return [
            'forceReload'=>'#crud-datatable-pjax',
            'title'=> "исполнитель #".$id,
            'content'=>'<span class="text-success">Добавлен в черный список</span>',
            'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
        ];
    }

    /**
     * Добавляет проект исполнителю
     * @param $doer_id
     * @return array
     */
    public function actionTakeProject($doer_id)
    {
        $request = Yii::$app->request;
        $model = new AddProjectForm($doer_id);
        $doer = Doer::findOne($doer_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить проект",
                    'content'=>$this->renderAjax('take_project', [
                        'model' => $model,
                        'doer' => $doer,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Привязать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->addProject()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить проект",
                    'content'=>'<span class="text-success">Заказ успешно привязан к исполнителю</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }else{
                return [
                    'title'=> "Добавить проект",
                    'content'=>$this->renderAjax('take_project', [
                        'model' => $model,
                        'doer' => $doer,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Привязать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRemoveProject($doer_id, $project_id)
    {
        $request = Yii::$app->request;
        $doerProject = DoerProject::find()->where(['doer_id' => $doer_id, 'project_id' => $project_id])->one();

        if($doerProject == null)
            throw new NotFoundHttpException('Запись не найдена');

        $doerProject->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing Doer model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить исполнителя #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
//                Yii::info($request->post()['Doer']['main_professions'], 'doer');




                $professionsInput [] = array();

                $professionsInput = $request->post()['Doer']['main_professions'] != null ? $request->post()['Doer']['main_professions'] : [];
                $otherProfessions = $request->post()['Doer']['other_professions'] != null ? $request->post()['Doer']['other_professions'] : [];
                $mergedProfessions = array_merge($professionsInput, $otherProfessions);
                $marks = Mark::find()->where(['doer_id' => $model->id])->andWhere(['not in','profession_id',$mergedProfessions])->all();
                foreach ($marks as $mark){
                    $mark->delete();
                }
                Yii::info($otherProfessions, 'doer');
                if ($otherProfessions!=null)
                    foreach ($otherProfessions as $otherProfession) {
                        $professionsInput [] = $otherProfession;
                    }
                Yii::info($professionsInput, 'doer');

                $allDoersMiddleMarks = DoerMiddleMarks::find()->where(['doer_id' => $model->id])->all();

                $deleteMiddleMarks = [];
                foreach ($allDoersMiddleMarks as $middleMark) {
//                    Yii::info($middleMark, 'doer');
                    $toDelete = $middleMark;

                    foreach ($professionsInput as $prof) {
                        if ($middleMark->profession_id == $prof) {
                            $toDelete = null;
                        }
                    }

                    if ($toDelete != null) {
                        $deleteMiddleMarks[] = $toDelete;
                    }
                }


                foreach ($deleteMiddleMarks as $deleteMiddleMark) {
//                    Yii::info($deleteMiddleMark, 'doer');
                    $deleteMiddleMark->delete();
                }



                foreach ($professionsInput as $prof) {
                    $isMarkExist = DoerMiddleMarks::find()->where(['doer_id' => $model->id, 'profession_id' => $prof])->all();
                    $count = 0;
                    if ($isMarkExist == null) {

                        $newMark = new DoerMiddleMarks();
                        $newMark->middle_mark = 0;
                        $newMark->profession_id = $prof;
                        $newMark->doer_id = $id;
                        $newMark->save();

                    }
                }

                $allMarks = Mark::find()->where(['doer_id' => $model->id])->asArray()->all();

                $professionsArray = [];
                foreach ($allMarks as $mark) {
//                    Yii::info($mark['project_id'], 'doer');
                    if(!isset($professionsArray[$mark['project_id']]))
                    {
                        $professionsArray[$mark['project_id']] = [$mark];
                    }
                    else {
                        array_push($professionsArray[$mark['project_id']], $mark);
                    }
                }

//                if ($isMarkExist == null) {

                $criteries = Criterions::find()->all();
//                    foreach ( $professionsInput as $inputProf ) {
//                        $isMarkExist = false;
//                        foreach ($professionsArray as $profession) {
////                            Yii::info($profession, 'doer');
//                            foreach ($profession as $p) {
//                                if ($p['profession_id'] == $inputProf) {
//                                    $isMarkExist = true;
//                                }
//                            }
//
//                            if ($isMarkExist == false) {
////                                Yii::info('ADD RECORD TO MARKS', 'doer');
//
////                                for ($i=1; $i<6; $i++) {
//                                foreach ($criteries as $critery) {
//                                    $newMark = new Mark();
//                                    $newMark->mark = 0;
//                                    $newMark->criterion_id = $critery->id;
//                                    $newMark->project_id = $profession[0]['project_id'];
//                                    $newMark->profession_id = $inputProf;
//                                    $newMark->doer_id = $id;
//                                    Yii::info($newMark->profession_id, 'doer');
//                                    $newMark->save();
//                                }
//                            }
//                        }
//
//                    }


//                    foreach ($professionsArray as $prof ) {
//                        $count++;
//                        Yii::info($prof[0]['project_id'], 'doer');
//                        $newMark = new Mark();
//                        $newMark->mark = 0;
//                        $newMark->criterion_id = 1;
//                        $newMark->project_id = $prof[0]['project_id'];
//                        $newMark->profession_id = $professionsInput[1];
//                        $newMark->doer_id = $id;
//                        $newMark->save();
////                        Yii::info('ADD NEW RECORD TO marks ' . $professionsArray, 'doer');
//
//                    }
//                }

//                    Yii::info($professionsArray[2][0]['profession_id'], 'doer');





                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "исполнитель #".$id,
                    'content'=>'<span class="text-success">Изменения сохранены</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                 return [
                    'title'=> "Изменить исполнителя #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Doer model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

     /**
     * Delete multiple existing Doer model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Doer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
